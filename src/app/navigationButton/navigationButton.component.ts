import { CardService } from '../services/cardService/card.service';
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'navigationButton',
  templateUrl: './navigationButton.component.html',
  styleUrls: ['./navigationButton.component.css']
})
export class NavigationButtonComponent implements OnInit {

  @Output() public prev = new EventEmitter<string>();
  @Output() public next = new EventEmitter<string>();
  @Input() public previous: boolean;
  @Input() public page: number;
  totalPage: number;


  constructor(private cardService: CardService) { }

  ngOnInit() {
    this.totalPage = Math.ceil(parseInt(this.cardService.TotalPages(), 10)/10);
  }


  previousPage(){
    this.prev.emit();
  }

  nextPage(){
    this.next.emit();
  }

}
