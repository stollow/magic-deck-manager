import { CardModel } from '../../models/CardModel';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';

@Injectable()
export class CardService {

    first: any;
    last: any;
    prev: any;
    next: any;
    totalPage: any;

    constructor(private http: HttpClient) { }

    getCards(Link): Observable<CardModel[]> {
        this.getPages(Link).subscribe(response =>{
           const link = this.parse_link_header(response.headers.get('Link'))
           this.first = link["first"];
           this.last = link["last"];
           this.prev = link["prev"];
           this.next = link["next"];
           this.totalPage = response.headers.get('Total-Count');
        })
        return this.request(Link);
    }

    getOptionsValue(value){
      return this.http.get<any>(`https://api.magicthegathering.io/v1/${value}`)
      .pipe(tap(res => {
        return res;
      }));
    }

    public TotalPages(){
        return this.totalPage;
    }

    public NextLink(){
        return this.next;
    }

    public PreviousLink(){
        return this.prev;
    }

    public request(Link){
        return this.http.get<CardModel[]>(
            `${Link}`)
        .pipe(map(result => result)
        );
    }

    public getPages(Link){

        return this.http.get<any>(Link
        ,{ observe: 'response' }).pipe(tap(res => {
          return res;
        }));
   }

   parse_link_header(header) {
    if (header.length == 0) {
      return ;
    }

    let parts = header.split(',');
    var links = {};
    parts.forEach( p => {
      let section = p.split(';');
      var url = section[0].replace(/<(.*)>/, '$1').trim();
      var name = section[1].replace(/rel="(.*)"/, '$1').trim();
      links[name] = url;

    });
    return links;
  }  
}
