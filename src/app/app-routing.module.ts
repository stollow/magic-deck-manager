import { DeckComponent } from './deck/deck.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardComponent } from './card/card.component';
import { SearchLayoutComponent } from './SearchLayout/SearchLayout.component';


const routes: Routes = [
  { path: 'listCards', component: SearchLayoutComponent},
  { path: 'Card', component: CardComponent },
  { path: 'deck', component: DeckComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
