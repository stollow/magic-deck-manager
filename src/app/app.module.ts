import { CardService } from './services/cardService/card.service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchLayoutComponent } from './SearchLayout/SearchLayout.component';
import { CardComponent } from './card/card.component';
import { NavigationButtonComponent } from './navigationButton/navigationButton.component';
import { CardSelectorFormComponent } from './CardSelectorForm/CardSelectorForm.component';
import { CardModalComponent } from './cardModal/cardModal.component';
import { CardLayoutComponent } from './cardLayout/cardLayout.component';
import { DeckComponent } from './deck/deck.component';


@NgModule({
   declarations: [
      AppComponent,
      SearchLayoutComponent,
      CardComponent,
      NavigationButtonComponent,
      CardSelectorFormComponent,
      CardModalComponent,
      CardLayoutComponent,
      DeckComponent
   ],
   imports: [
      HttpClientModule,
      BrowserModule,
      AppRoutingModule,
      FormsModule,
      BrowserAnimationsModule,
      AngularMaterialModule
   ],
   providers: [
      CardService
   ],
   bootstrap: [
      AppComponent
   ],
   schemas: [
      CUSTOM_ELEMENTS_SCHEMA
   ],
   entryComponents: [
      CardModalComponent
   ]
})
export class AppModule { }
