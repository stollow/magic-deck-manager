import { CardModel } from './../models/CardModel';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CardModalComponent } from '../cardModal/cardModal.component';

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() card: CardModel;
  @Input() edit: boolean;
  @Output() deck = new EventEmitter<CardModel>();

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  onImageClick() {
    if (!this.edit) {
      this.openDialog();
    } else {
     this.deck.emit(this.card);
    }
  }

  addToDeck(){

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CardModalComponent, {
      width: '500px',
      height:'500px',
      data: { card: this.card }
    });
  }

}
