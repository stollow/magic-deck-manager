import { CardModel } from '../models/CardModel';
import { CardService } from '../services/cardService/card.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'searchLayout',
  templateUrl: './SearchLayout.component.html',
  styleUrls: ['./SearchLayout.component.css']
})
export class SearchLayoutComponent implements OnInit {

  cards: CardModel[] = [];
  deck: CardModel[] = [];
  page = 1;
  previous = false;
  isLoading: boolean;
  edition = false;

  constructor(private cardService: CardService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.deck = JSON.parse(localStorage.getItem('deck'));
    this.requestCard();
  }

  requestCard() {
    this.isLoading = true;
    this.cardService.getCards(
          `https://api.magicthegathering.io/v1/cards?pageSize=10`)
          .subscribe(response => {
            this.isLoading = false;
            this.cards = response;
        });
  }

  addToDeck(card: CardModel) {
    this.deck.push(card);
    localStorage.setItem('deck', JSON.stringify(this.deck));
  }

  navigateToNext() {
    this.page += 1;
    this.isLoading = true;
    this.previous = true;
    this.cardService.getCards(`${this.cardService.NextLink()}`).subscribe(response => {
      this.isLoading = false;
      this.cards = response;
    });
  }

  navigateToPrev() {
    this.page -= 1;
    this.isLoading = true;
    this.cardService.getCards(`${this.cardService.PreviousLink()}`).subscribe(response => {
      this.cards = response;
      if (!this.cardService.prev) {
        this.isLoading = false;
        this.previous = false;
      }
    });
  }

  search(form) {
    this.isLoading = true;
    this.cardService.getCards(
          `https://api.magicthegathering.io/v1/cards?pageSize=10&${form.value.filter}=${form.value.filterValue.trim()}`)
          .subscribe(response => {
            this.isLoading = false;
            this.cards = response;
        });
  }
}
