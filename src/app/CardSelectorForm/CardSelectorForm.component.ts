import { CardService } from '../services/cardService/card.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'CardSelectorForm',
  templateUrl: './CardSelectorForm.component.html',
  styleUrls: ['./CardSelectorForm.component.css']
})
export class CardSelectorFormComponent implements OnInit {

  @Output() public formSending = new EventEmitter<NgForm>();
  selectValue: string[] = [];
  openValue = true;
  loading = false;

  constructor(private cardService: CardService) { }

  ngOnInit() {
  }

  onSubmit(formContent: NgForm){
    this.formSending.emit(formContent);
  }

  onOptionsSelected(value) {
    this.loading = true;
    if (value !== 'name') {
      this.openValue = false;
      this.cardService.getOptionsValue(value).subscribe(response => {
        this.selectValue = response[value];
        this.loading = false;
      });
    } else {
      this.openValue = true;
      this.loading = false;
    }
  }

}
