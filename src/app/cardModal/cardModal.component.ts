import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { CardModel } from '../models/CardModel'


@Component({
  selector: 'cardModal',
  templateUrl: './cardModal.component.html',
  styleUrls: ['./cardModal.component.css']
})
export class CardModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CardModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CardModel) 
  { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }
}
