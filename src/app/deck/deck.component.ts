import { Component, OnInit } from '@angular/core';
import { CardModel } from '../models/CardModel';

@Component({
  selector: 'deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.css']
})
export class DeckComponent implements OnInit {

  cards: CardModel[] = [];

  constructor() { }

  ngOnInit() {
    this.cards = JSON.parse(localStorage.getItem('deck'));
  }

  addToDeck(card) {
    console.log(card);
  }

  deleteCardFromDeck(card) {
    this.cards = this.cards.filter(mappedCard => mappedCard !== card);
    localStorage.setItem('deck', JSON.stringify(this.cards));
  }

}
