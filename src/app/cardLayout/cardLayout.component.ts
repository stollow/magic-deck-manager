import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { CardModel } from '../models/CardModel';

@Component({
  selector: 'cardLayout',
  templateUrl: './cardLayout.component.html',
  styleUrls: ['./cardLayout.component.css']
})
export class CardLayoutComponent implements OnInit {

  @Input() cards: CardModel[];
  edition: boolean;
  @Output() deck = new EventEmitter<CardModel>();

  constructor() { }

  ngOnInit() {
  }

  addToDeck(card){
    this.deck.emit(card);
  }

  switchEditDescription() {
    this.edition = !this.edition;
  }

}
