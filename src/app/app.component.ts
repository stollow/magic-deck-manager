import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CardModalComponent } from './cardModal/cardModal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pokemon-tcg-app';
  name: string;
  color: string;


  constructor() { }
}
